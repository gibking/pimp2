import logging
import time
import traceback
from media_player_controller import MediaPlayerController

logger = logging.getLogger(__name__)


class Frontend:

    def __init__(self, media_player_controller: MediaPlayerController):
        self.mediaPlayerController = media_player_controller

        logger.debug("Setting up commands")
        self.command_map = {
            '+': media_player_controller.volume_up,
            '-': media_player_controller.volume_down,
            't': media_player_controller.pause,
            'n': media_player_controller.track_next,
            'p': media_player_controller.track_prev,
            'q': media_player_controller.shutdown,
            'h': self.__help,
        }

    def __help(self):
        print('Valid commands:')
        print('\n'.join([f'{k} -> {v}' for k, v in self.command_map.items()]))

    def run(self):
        while True:
            try:
                cmd = input('> ')
                if not cmd:
                    continue
                cmd_function = self.command_map.get(cmd, self.mediaPlayerController.play_binding)
                logger.debug(f'invoking {cmd_function}')
                t = time.monotonic()
                if cmd_function == self.mediaPlayerController.play_binding:
                    self.mediaPlayerController.play_binding(cmd)
                else:
                    cmd_function()
                logger.debug(f'took {(time.monotonic() - t) * 1000} ms')
            except Exception as e:
                logger.error("Unhandled error:")
                logger.error(traceback.print_exc())
