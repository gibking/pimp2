Dependencies (Packages)
------------

- python3 (3.7 on Debian Buster)
- vlc
- spi enabled

      raspi-config

  or

      echo dtparam=spi=on >> /boot/config.txt
      reboot

Pi Setup
--------

    sudo ./install.sh

Configuration
--------
Put systemd drop-in file into /etc/systemd/system/pimp2.service.d/ and override/set environment variables there


Debug Run VLC
--------

    vlc -I http --http-password $(cat /etc/machine-id) --no-video --aout=alsa --no-metadata-network-access

Wiring
------

|Pi pin#|(Color) |Component|
---| ----   | ---
5  | white  | yellow button A
6  | gray   | all LEDs -
8  | orange | red LED +
9  | black  | MFRC522 GND
13 | yellow | yellow button A
14 | green  | yellow button B
15 | red    | MFRC522 3.3V
16 | yellow | yellow LED +
18 | green  | green LED +
19 | gray   | MFRC522 MOSI
21 | white  | MFRC522 MISO
22 | brown  | MFRC522 RST
23 | purple | MFRC522 SCK
24 | blue   | MFRC522 SDA
29 | green  | green button A
30 | white  | green button B
31 | blue   | blue button A
39 | white  | blue button B
