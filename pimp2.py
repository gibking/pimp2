#!/usr/bin/env python3
import logging
import os
import sys
import threading

from media_player_controller import MediaPlayerController

if __name__ == '__main__':

    log_level = os.getenv('PIMP_LOG_LEVEL', 'info')
    numeric_level = getattr(logging, log_level.upper(), None)
    if not isinstance(numeric_level, int):
        raise ValueError(f"Invalid log level: {log_level}")
    logging.basicConfig(level=numeric_level, format='%(asctime)s %(levelname)-8s %(name)s: %(message)s')

    binding_dir = os.getenv('PIMP_BINDING_DIR')
    if not binding_dir:
        raise RuntimeError('PIMP_BINDING_DIR not set')

    logging.info(f"starting with environment:")
    for k, v in os.environ.items():
        if k.startswith('PIMP_'):
            logging.info(f"{k}={v}")
    ctrl = MediaPlayerController(binding_dir)

    # Init GPIO Frontend
    if not os.getenv('PIMP_NO_GPIO'):
        import gpio_frontend

        logging.getLogger('gpio_frontend.card_reader').setLevel(logging.INFO)
        logging.getLogger('gpio_frontend.button').setLevel(logging.INFO)
        logging.getLogger('gpio_frontend.led').setLevel(logging.INFO)
        gpio_fe = gpio_frontend.Frontend(ctrl)
        threading.Thread(target=gpio_fe.run).start()

    if sys.stdout.isatty():
        import repl_frontend

        repl_fe = repl_frontend.Frontend(ctrl)
        threading.Thread(target=repl_fe.run).start()
    else:
        logging.info("not running on a tty. not loading REPL frontend")
