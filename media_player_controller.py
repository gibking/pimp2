import distutils.util
import json
import logging
import os
import threading
import time
import urllib.parse
from enum import Enum

import requests
from xmltodict3 import XmlTextToDict

logger = logging.getLogger(__name__)


class PlayState(Enum):
    playing = 1
    paused = 2
    stopped = 3


class MediaPlayerController:

    def __init__(self, binding_dir):
        self.session = requests.Session()
        self.binding_dir = binding_dir
        password = os.getenv('PIMP_VLC_PASSWORD')
        if not password:
            raise RuntimeError('No PIMP_VLC_PASSWORD set')
        logger.debug(f"using password '{password}' for vlc auth")
        logging.getLogger('urllib3.connectionpool').setLevel(logging.INFO)
        self.session.auth = ('', password)
        self.baseurl = 'http://localhost:8080/requests'
        self.current_binding = {}
        self.player_status = {}
        self.state_changed_callback = None
        self.volume_max = int(os.getenv('PIMP_VOLUME_MAX', 256))
        self.volume_step = int(os.getenv('PIMP_VOLUME_STEP', 10))
        self.seek_on_pause = os.getenv('PIMP_SEEK_ON_PAUSE', '-3')
        self.volume_default()

        threading.Thread(target=self.__monitor_state).start()
        threading.Thread(target=self.__idle_shutdown).start()

    def shutdown(self):
        logger.info("shutting down")
        self.stop()
        os.system("sudo systemctl poweroff")
        time.sleep(5)
        logger.error(f"Cannot power off. trying with -f")
        os.system("sudo systemctl poweroff -f")
        time.sleep(5)
        logger.error(f"Cannot power off. trying with -ff")
        time.sleep(5)
        os.system("sudo systemctl poweroff -ff")
        logger.error(f"This should really not happen")

    def __idle_shutdown(self):
        idle_shutdown_delay = int(os.getenv('PIMP_IDLE_SHUTDOWN_DELAY', 900))
        if idle_shutdown_delay <= 0:
            logger.info('disabling idle shutdown')
            return
        logger.info(f'will shut down after {idle_shutdown_delay} seconds')
        last_active = time.monotonic()
        while last_active > time.monotonic() - idle_shutdown_delay:
            current_playing_state = self.player_status.get('state')
            if current_playing_state == PlayState.playing:
                last_active = time.monotonic()
            time.sleep(0.5)
        logger.info(f"being idle for {idle_shutdown_delay} seconds")
        if logger.getEffectiveLevel() == logging.DEBUG:
            logger.warning(f"not powering of in debug mode")
        else:
            logger.info(f"poweroff after {idle_shutdown_delay} seconds")
            self.shutdown()

    def update_state(self):
        response = self.session.get(f'{self.baseurl}/status.xml')
        if response.status_code == 200:
            raw_status = XmlTextToDict(response.text,
                                       ignore_namespace=True).get_dict()
            new_player_status = {
                'state': PlayState[raw_status['root']['state']],
                'volume': int(float(raw_status['root']['volume'])),
                'random': bool(distutils.util.strtobool(raw_status['root']['random'])),
            }
            if new_player_status != self.player_status:
                self.player_status = new_player_status
                logger.debug(f'state changed to: {self.player_status}')
                if self.state_changed_callback:
                    self.state_changed_callback(self.player_status)
        else:
            raise RuntimeWarning(response.text)

    def __monitor_state(self):
        while True:
            try:
                self.update_state()
                # limit rate to ~5 per second
                time.sleep(0.2)
            except RuntimeWarning as w:
                logger.warning(w)
                time.sleep(1)

    def __send_command(self, command, **kwargs):
        if len(kwargs) > 0:
            args = "&".join(f'{urllib.parse.quote(k)}={urllib.parse.quote(v)}' for (k, v) in kwargs.items())
            url = f'{self.baseurl}/status.xml?command={command}&{args}'
        else:
            url = f'{self.baseurl}/status.xml?command={command}'
        response = self.session.get(url)
        logger.debug(f'request for {url} returned {response.status_code}')
        if response.status_code != 200:
            logger.warning(response.text)

    def play_binding(self, binding_id):
        logger.info(f'should play binding {binding_id}')
        if binding_id != self.current_binding.get('id') or self.player_status.get('random'):
            binding = self.get_binding_data(binding_id)
            if binding:
                self.__send_command('pl_empty')
                # toggle random play if required
                if binding['random'] != self.player_status.get('random'):
                    self.__send_command('pl_random')
                self.current_binding = binding
                logger.debug(f"loading playlist {binding['playlist']}")
                self.__send_command('in_play', input=binding['playlist'])
        else:
            self.resume()

    def get_binding_data(self, binding_id: str) -> dict:
        binding_file = os.path.join(self.binding_dir, f'{str(binding_id)}.json')
        if not os.path.normpath(binding_file).startswith(self.binding_dir):
            raise ValueError(f"Bindings outside 'binding_directory' not supported")
        try:
            with open(binding_file, 'r') as f:
                binding_data = json.load(f)
                binding_data['id'] = binding_id
                binding_data['random'] = bool(distutils.util.strtobool(binding_data.get('random', 'false')))
                logger.debug(f"Loaded binding from {binding_file}:{binding_data}")
                return binding_data
        except Exception as e:
            logger.error(f"Error loading {binding_file}: {e}")

    def stop(self):
        logger.info('stop')
        self.__send_command('pl_stop')

    def pause(self):
        if self.player_status.get('state') == PlayState.playing:
            logger.info('pausing')
            self.__send_command('pl_pause')
            self.__send_command('seek', val=self.seek_on_pause)

    def resume(self):
        if self.player_status.get('state') == PlayState.paused:
            logger.info('resuming')
            self.__send_command('pl_pause')

    def track_prev(self):
        logger.info('previous track')
        self.__send_command('pl_previous')

    def track_next(self):
        logger.info('next track')
        self.__send_command('pl_next')

    def volume_up(self):
        if self.player_status.get('volume') <= self.volume_max - self.volume_step:
            logger.info('volume up')
            self.__send_command('volume', val=f'+{self.volume_step}')

    def volume_down(self):
        logger.info('volume down')
        self.__send_command('volume', val=f'-{self.volume_step}')

    def volume_set(self, level):
        self.__send_command('volume', val=min(level, self.volume_max))

    def volume_default(self):
        logger.info('setting default volume')
        self.__send_command('volume', val=f"{str(os.getenv('PIMP_VOLUME_DEFAULT', self.volume_max * 0.6))}")
