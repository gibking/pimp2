#!/usr/bin/env python3

import atexit
import logging
import threading

from media_player_controller import MediaPlayerController, PlayState
from .button import Button
from .card_reader import CardReader
from .led import LED

""" left button GPIO input pin (BCM numbering)"""
button_left_pin = 5
""" right button GPIO input pin (BCM numbering)"""
button_right_pin = 6
""" middle button GPIO input pin (BCM numbering)"""
button_middle_pin = 27

""" state_led pin (BCM numbering)"""
ready_led_pin = 23
playing_led_pin = 24

logger = logging.getLogger(__name__)


class Frontend:

    def __init__(self, media_player_controller: MediaPlayerController):
        self.media_player_controller = media_player_controller
        logger.debug("Setting up LEDs")
        self.ready_led = LED(ready_led_pin)
        self.ready_led.blink(0.25, 0.25)
        self.playing_led = LED(playing_led_pin)

        logger.debug("Setting up cardreader")

        self.card_reader = CardReader(media_player_controller.play_binding, media_player_controller.pause,
                                      media_player_controller.update_state)

        logger.debug("Setting up buttons")
        self.button_left = Button(button_left_pin, media_player_controller.volume_down,
                                  media_player_controller.track_prev)
        atexit.register(self.button_left.cleanup)
        self.button_right = Button(button_right_pin, media_player_controller.volume_up,
                                   media_player_controller.track_next)
        atexit.register(self.button_right.cleanup)
        self.button_middle = Button(button_middle_pin, media_player_controller.volume_default, self.shutdown_hook,
                                    long_press_duration=4)
        atexit.register(self.button_middle.cleanup)

        media_player_controller.state_changed_callback = self.state_changed

        self.last_playback_state = None

    def state_changed(self, mediaplayer_state):
        if mediaplayer_state.get('state') != self.last_playback_state:
            if mediaplayer_state.get('state') == PlayState.playing:
                self.playing_led.on()
            elif mediaplayer_state.get('state') == PlayState.stopped:
                self.playing_led.off()
            elif mediaplayer_state.get('state') == PlayState.paused:
                self.playing_led.blink(0.5, 0.5)
        self.last_playback_state = mediaplayer_state.get('state')

    def run(self):
        logger.debug("starting card loop")
        threading.Thread(target=self.card_reader.loop).start()
        atexit.register(self.card_reader.cleanup)
        self.ready_led.on()

    def stop(self):
        self.ready_led.blink(0.25, 0.25)
        logger.debug("stopping card loop")
        self.card_reader.stop()

    def shutdown_hook(self):
        logger.info("shutdown by button triggered")
        self.stop()
        self.media_player_controller.shutdown()
