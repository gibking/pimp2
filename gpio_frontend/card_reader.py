import time
from mfrc522 import SimpleMFRC522
import binascii
import logging
import atexit
import RPi.GPIO as GPIO

logger = logging.getLogger(__name__)

"""CardReader Class with callbacks"""


class CardReader:
    # Generate ID the same was a USB RFID reader does
    class BasicMFRC522(SimpleMFRC522):
        def uid_to_num(self, uid):
            # hex representation of the first four bytes in reversed order
            return bytearray(reversed(uid[:4:])).hex()

    def __init__(self, new_card_cb, remove_cb, change_cb=None):
        self.on_remove_cb = remove_cb
        self.on_new_card_cb = new_card_cb
        self.on_change_cb = change_cb
        self.shouldStop = False
        GPIO.setmode(GPIO.BCM)
        # Readers 3.3V is connected to BCM 22
        GPIO.setup(22, GPIO.OUT)
        GPIO.output(22, GPIO.HIGH)
        self.reader = self.BasicMFRC522()
        atexit.register(self.cleanup)

    def cleanup(self):
        self.stop()
        logger.debug(f"Cleaning up card reader RST on BCM PIN 15")
        GPIO.cleanup(15)  # RST-PIN of MFRC522 reader
        GPIO.output(22, GPIO.LOW)  # Poweroff reader
        GPIO.cleanup(22)

    def stop(self):
        self.shouldStop = True

    def loop(self):
        sleep_time = 0.02
        last_card = None
        previous_id = None
        empty = True
        while not self.shouldStop:
            time_before = time.perf_counter()
            current_id = self.reader.read_id_no_block()
            # logger.debug(f"read: {current_id} in {(time.perf_counter() - time_before) * 1000:.0f}ms")
            if current_id is not None:
                if current_id == last_card and not empty:
                    pass
                    # logger.debug("Same card. Sleeping 0.01s")
                    sleep_time = 0.01
                else:
                    empty = False
                    self.on_new_card_cb(current_id)
                    last_card = current_id
                    self.on_change_cb()
            else:
                if not empty and previous_id is None and last_card is not None:
                    # logger.debug("Card vanished, Sleeping .02s")
                    sleep_time = 0.02
                    empty = True
                    self.on_remove_cb()
                    self.on_change_cb()
            previous_id = current_id
            time.sleep(sleep_time)
