import RPi.GPIO as GPIO
import logging
import time
import threading
import atexit


class LED:

    def __init__(self, bcm_pin, initial_on=False):
        self.logger = logging.getLogger(f"{__name__}.PIN_{bcm_pin}")
        self.pin = bcm_pin
        self._blink_thread = None
        self._blink_should_stop = False
        GPIO.setmode(GPIO.BCM)
        self.logger.debug(f"Setting up LED")
        GPIO.setup(self.pin, GPIO.OUT)
        atexit.register(self._cleanup)
        if initial_on:
            self.on()

    def _cleanup(self):
        self.logger.debug(f"Cleaning up LED")
        GPIO.cleanup(self.pin)

    def on(self):
        self.logger.info(f"on")
        self._terminate_blink()
        GPIO.output(self.pin, GPIO.HIGH)

    def off(self):
        self.logger.info(f"off")
        self._terminate_blink()
        GPIO.output(self.pin, GPIO.LOW)

    def _terminate_blink(self):
        if self._blink_thread:
            self.logger.debug(f"waiting for blinking to end")
            self._blink_should_stop = True
            self._blink_thread.join()
            self._blink_thread = None

    def blink(self, on_s, off_s):
        self._terminate_blink()
        self.logger.info(f"start blinking")
        self._blink_should_stop = False
        self._blink_thread = threading.Thread(target=self._continuous_blink, args=(on_s, off_s))
        self._blink_thread.start()

    def _continuous_blink(self, on_s, off_s):
        while not self._blink_should_stop:
            GPIO.output(self.pin, GPIO.HIGH)
            t = on_s
            while t > 0:
                t -= on_s / 10
                if self._blink_should_stop:
                    return
                time.sleep(on_s / 10)
            GPIO.output(self.pin, GPIO.LOW)
            t = off_s
            while t > 0:
                t -= off_s / 10
                if self._blink_should_stop:
                    return
                time.sleep(off_s / 10)
        self.logger.debug(f"stop blinking")
