import threading

import RPi.GPIO as GPIO
import logging
import atexit
import time


class Button:

    def __init__(self, bcm_pin, short_press_callback, long_press_callback=None, long_press_duration=1.5, hz=50):
        self.logger = logging.getLogger(f"{__name__}.PIN_{bcm_pin}")
        self._pin = bcm_pin
        self._short_press_action = short_press_callback
        self._long_press_action = long_press_callback
        self._long_press_duration = long_press_duration
        self._timer = None
        GPIO.setmode(GPIO.BCM)
        GPIO.setup(self._pin, GPIO.IN, pull_up_down=GPIO.PUD_UP)
        atexit.register(self.cleanup)
        self._poller_hz = hz
        self._poller = threading.Thread(target=self._poll)
        self._poller_should_stop = False
        self._poller.start()
        self.logger.debug(f"created. polling every {1 / self._poller_hz}s")

    def cleanup(self):
        self.logger.debug("cleaning up Button")
        self._poller_should_stop = True
        self._poller.join()
        GPIO.cleanup(self._pin)

    def _execute_short_action(self):
        self.logger.debug(f"async executing short-press action:{self._short_press_action}")
        threading.Thread(target=self._short_press_action).start()

    def _execute_long_action(self):
        self._timer = None
        if self._long_press_action:
            self.logger.debug(f"async executing long-press action:{self._long_press_action}")
            threading.Thread(target=self._long_press_action).start()
        else:
            self.logger.debug(f"no long-press action defined")

    def _poll(self):
        last_state = None
        while not self._poller_should_stop:
            current_state = GPIO.input(self._pin)
            if current_state != last_state:
                if current_state == GPIO.LOW:
                    self.logger.debug("pressed")
                    self._cancel_timer()
                    self._timer = threading.Timer(self._long_press_duration, self._execute_long_action)
                    self._timer.start()
                else:
                    self.logger.debug("released")
                    if self._timer:
                        self._cancel_timer()
                        self._execute_short_action()
            last_state = current_state
            time.sleep(1 / self._poller_hz)
        self.logger.debug(f"stop polling")

    def _cancel_timer(self):
        if self._timer:
            self.logger.debug("Timer still running, cancelling")
            self._timer.cancel()
            self._timer = None
