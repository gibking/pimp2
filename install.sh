#!/bin/bash

if [[ "$1" != "-u" ]]; then
  echo "checking/installing dependencies"
  sudo apt install python3-pip vlc
  sudo pip3 install -r requirements.txt

  id pimp2 &>/dev/null || sudo useradd -d /opt/pimp2 -m -r -G spi,i2c,gpio,audio,kmem pimp2
  if ! sudo -u pimp2 test -r /srv/pimp/{bindings,music}; then
    echo "setting up /srv/pimp"
    sudo mkdir -p /srv/pimp/{bindings,music} 2>/dev/null
    sudo chown -R pimp2:pimp2 /srv/pimp
  fi
else
  echo "update only"
fi

echo "installing to /opt/pimp2"
sudo cp -af $(dirname $(readlink -f "$0"))/* /opt/pimp2
sudo chown -R root:pimp2 /opt/pimp2

echo "setting sudo permission for poweroff"
echo "pimp2 ALL=(ALL) NOPASSWD:/bin/systemctl poweroff *" | sudo tee /etc/sudoers.d/pimp2_reboot

echo "installing & enabling services"
sudo sh -c "sed s/%PASSWORD%/$(cat /etc/machine-id)/g /opt/pimp2/systemd/vlc.service.tpl > /usr/lib/systemd/system/vlc.service"
sudo sh -c "sed s/%PASSWORD%/$(cat /etc/machine-id)/g /opt/pimp2/systemd/pimp2.service.tpl > /usr/lib/systemd/system/pimp2.service"
sudo systemctl daemon-reload
sudo systemctl enable vlc.service
sudo systemctl enable pimp2.service

echo "(re)starting services"
sudo systemctl restart vlc.service
sudo systemctl restart pimp2.service