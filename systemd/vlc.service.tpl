[Unit]
Description=VLC Media Player
DefaultDependencies=no


[Service]
User=pimp2
Nice=-10
ExecStart=/usr/bin/vlc -I http --http-password %PASSWORD% --no-video --aout=alsa --no-metadata-network-access
Restart=always

[Install]
WantedBy=local-fs.target
