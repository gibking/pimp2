[Unit]
Description=PIMP2
DefaultDependencies=no
Requires=vlc.service
After=local-fs.target

[Service]
User=pimp2
Restart=always
Nice=-5
Environment=PIMP_LOG_LEVEL=info
Environment=PIMP_BINDING_DIR=/srv/pimp/bindings
Environment=PIMP_VOLUME_MAX=256
Environment=PIMP_VOLUME_STEP=10
Environment=PIMP_IDLE_SHUTDOWN_DELAY=900
Environment=PIMP_VLC_PASSWORD=%PASSWORD%
ExecStart=/usr/bin/python3 /opt/pimp2/pimp2.py

[Install]
WantedBy=sound.target
